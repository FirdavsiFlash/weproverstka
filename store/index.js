import Vuex from 'vuex'
import Vue from 'vue'
 


Vue.use(Vuex)

const state = () => ({

})

const getters = {
  ApiData: state => state.ApiData
}

const mutations = {}

const actions = {

}
const modules = {
 

}
export default {
  state,
  getters,
  actions,
  mutations,
  modules
}
